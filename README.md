Murano LAMP Base
================

This is the source for the LAMP Base Murano package running on the NeCTAR cloud.

Use this package if you do not want to configure MySQL. Use the LAMP package if you do.

A Makefile is included to help the build process.

You will need your NeCTAR cloud credentials loaded and the Murano CLI tools
available in your path.

You will also need the following required packages:

* [DuplyBackups](https://bitbucket.org/qcif-murano/murano-duply-backups)
* [QRIScloudLib](https://bitbucket.org/qcif-murano/murano-qriscloud-lib)
